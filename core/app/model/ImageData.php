<?php

class ImageData
{
    public static $tablename = "image";

    public function ImageData()
    {
        $this->folder = "uploads/";
        $this->historial_id = "";
        $this->image = "";
        $this->description = "";
        $this->created_at = "NOW()";
    }


    public function add()
    {
        $sql = "insert into " . self::$tablename . " (description,historial_id,folder,src,created_at) ";
        $sql .= "value (\"$this->description\",\"$this->historial_id\",\"$this->folder\",\"$this->image\",NOW())";
        Executor::doit($sql);
    }

    public static function delById($id)
    {
        $sql = "delete from " . self::$tablename . " where id=$id";
        Executor::doit($sql);
    }

    public function del()
    {
        $sql = "delete from " . self::$tablename . " where id=$this->id";
        Executor::doit($sql);
    }

// partiendo de que ya tenemos creado un objecto MedicData previamente utilizamos el contexto
    public function update_active()
    {
        $sql = "update " . self::$tablename . " set last_active_at=NOW() where id=$this->id";
        Executor::doit($sql);
    }


    public function update()
    {
        $sql = "update " . self::$tablename . " set name=\"$this->name\",lastname=\"$this->lastname\",address=\"$this->address\",phone=\"$this->phone\",email=\"$this->email\",category_id=$this->category_id where id=$this->id";
        Executor::doit($sql);
    }

    public static function getById($id)
    {
        $sql = "select * from " . self::$tablename . " where id=$id";
        $query = Executor::doit($sql);
        return Model::one($query[0], new ImageData());
    }


    public static function getAll()
    {
        $sql = "select * from " . self::$tablename . " order by created_at desc";
        $query = Executor::doit($sql);
        return Model::many($query[0], new ImageData());
    }

    public static function getByIdHistorial($id)
    {
        $sql = "select * from " . self::$tablename . " where historial_id=$id" . " order by created_at desc";
        $query = Executor::doit($sql);
        return Model::many($query[0], new ImageData());
    }
    public static function getAllActive()
    {
        $sql = "select * from image where last_active_at>=date_sub(NOW(),interval 3 second)";
        $query = Executor::doit($sql);
        return Model::many($query[0], new ImageData());
    }

    public static function getAllUnActive()
    {
        $sql = "select * from image where last_active_at<=date_sub(NOW(),interval 3 second)";
        $query = Executor::doit($sql);
        return Model::many($query[0], new ImageData());
    }


    public function getUnreads()
    {
        return MessageData::getUnreadsByClientId($this->id);
    }


    public static function getLike($q)
    {
        $sql = "select * from " . self::$tablename . " where title like '%$q%' or email like '%$q%'";
        $query = Executor::doit($sql);
        return Model::many($query[0], new ImageData());
    }


    function insert_img($folder, $image)
    {

        $sql = "insert into " . self::$tablename . " (historial_id,folder,src,created_at) ";
        $sql .= "value (\"$this->historial_id\",\"$this->folder\",\"$this->image\",NOW())";
        Executor::doit($sql);

    }

    public static function getBySQL($sql)
    {
        $query = Executor::doit($sql);
        return Model::many($query[0], new ImageData());
    }
    /*
        function get_imgs(){

            $sql = "select * from image order by created_at desc";
            $query = Executor::doit($sql);
            return Model::many($query[0],new MedicData());

            $images = array();
             while($r=$query->fetch_object()){
                $images[] = $r;
            }
            return $images;
        }

        function get_img($id){
            $image = null;
            $con = con();
            $query=$con->query("select * from image where id=$id");
            while($r=$query->fetch_object()){
                $image = $r;
            }
            return $image;
        }

        function del_image($id){
            $con = con();
            $con->query("delete from image where id=$id");
        }
    */

}

?>