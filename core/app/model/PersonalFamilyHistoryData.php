<?php

/**
 * Created by Carlos Gerd CLaros.
 * User: User
 * Date: 11/15/2018
 * Time: 10:36 AM
 */
class PersonalFamilyHistoryData
{
    public static $tablename = "personalfamilyhistory";


    public function PersonalFamilyHistoryData()
    {
        $this->fathervivo = "";
        $this->fatherdiseases = "";
        $this->mothervivo = "";
        $this->motherdiseases = "";
        $this->numberbrother = "";
        $this->brotherdiseases = "";
        $this->other = "";
        $this->created_at = "NOW()";


    }

    public function getPacient()
    {
        return PacientData::getById($this->pacient_id);
    }

    public function getMedic()
    {
        return MedicData::getById($this->medic_id);
    }

    //  public function getStatus(){ return StatusData::getById($this->status_id); }


    public function add()
    {

        $sql = "insert into vitalsigns (fathervivo, fatherdiseases, mothervivo, motherdiseases, numberbrother, brotherdiseases, other, pacient_id , created_at,status_id) ";
        $sql .= "value (\"$this->fathervivo\",\"$this->fatherdiseases\",\"$this->mothervivo\",\"$this->motherdiseases\",\"$this->numberbrother\",\"$this->brotherdiseases\",\"$this->other\",\"$this->pacient_id\",$this->created_at,\"$this->status_id\")";
        //echo $sql;
        return Executor::doit($sql);
    }

    public static function delById($id)
    {
        $sql = "delete from " . self::$tablename . " where id=$id";
        Executor::doit($sql);
    }

    public function del()
    {
        $sql = "delete from " . self::$tablename . " where id=$this->id";
        Executor::doit($sql);
    }

// partiendo de que ya tenemos creado un objecto VitalSignsData previamente utilizamos el contexto
    public function update()
    {
        $sql = "update " . self::$tablename . " set name=\"$this->name\" where id=$this->id";
        Executor::doit($sql);
    }

    public static function getById($id)
    {
        $sql = "select * from " . self::$tablename . " where id=$id";
        $query = Executor::doit($sql);
        return Model::one($query[0], new PersonalFamilyHistoryData());
    }

    public static function getAll()
    {
        $sql = "select * from " . self::$tablename;
        $query = Executor::doit($sql);
        return Model::many($query[0], new PersonalFamilyHistoryData());

    }

    public static function getLike($q)
    {
        $sql = "select * from " . self::$tablename . " where name like '%$q%'";
        $query = Executor::doit($sql);
        return Model::many($query[0], new PersonalFamilyHistoryData());
    }


    public static function getRepeated($pacient_id, $medic_id, $date_at, $time_at)
    {
        $sql = "select * from " . self::$tablename . " where pacient_id=$pacient_id and medic_id=$medic_id and date_at=\"$date_at\" and time_at=\"$time_at\"";
        $query = Executor::doit($sql);
        return Model::one($query[0], new PersonalFamilyHistoryData());
    }

    public static function getBySQL($sql)
    {
        $query = Executor::doit($sql);
        return Model::many($query[0], new PersonalFamilyHistoryData());
    }

    public static function getAllPendings()
    {
        $sql = "select * from " . self::$tablename . " where date(date_at)>=date(NOW()) and status_id=1 order by date_at";
        $query = Executor::doit($sql);
        return Model::many($query[0], new PersonalFamilyHistoryData());
    }
}

?>