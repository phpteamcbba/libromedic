<?php

/**
 * Created by Carlos Gerd CLaros.
 * User: User
 * Date: 11/15/2018
 * Time: 10:36 AM
 */
class RecipeData
{
    public static $tablename = "recipe";


    public function RecipeData()
    {
        $this->pacient_id = "";
        $this->medic_id = "";
        $this->recipe = "";
        $this->recipe_pdf = "";
        $this->created_at = "NOW()";
        $this->date_at = "NOW()";
        $this->time_at = "NOW()";
        $this->historial_id = "";


    }

    public function getPacient()
    {
        return PacientData::getById($this->pacient_id);
    }


    public function getMedic()
    {
        return MedicData::getById($this->medic_id);
    }

    //  public function getStatus(){ return StatusData::getById($this->status_id); }


    public function add()
    {

        $sql = "insert into recipe (recipe,recipe_pdf, date_at, time_at, created_at, pacient_id,  medic_id,historial_id) ";
        $sql .= "value (\"$this->recipe\",\"$this->recipe_pdf\",$this->date_at,$this->time_at,$this->created_at,\"$this->pacient_id\",\"$this->medic_id\",\"$this->historial_id\")";
        // echo $sql;
        return Executor::doit($sql);
    }

    public static function delById($id)
    {
        $sql = "delete from " . self::$tablename . " where id=$id";
        Executor::doit($sql);
    }

    public function del()
    {
        $sql = "delete from " . self::$tablename . " where id=$this->id";
        Executor::doit($sql);
    }

    public function delupdate()
    {
        $sql = "update " . self::$tablename . " set status_id=0  where id=$this->id";
        //echo $sql ;
        Executor::doit($sql);
    }

// partiendo de que ya tenemos creado un objecto VitalSignsData previamente utilizamos el contexto


    public function update()
    {
        $sql = "update " . self::$tablename . " set recipe=\"$this->recipe\" where id=$this->id";
        Executor::doit($sql);
    }

    public static function getById($id)
    {
        $sql = "select * from " . self::$tablename . " where id=$id";
        //echo $sql;
        $query = Executor::doit($sql);
        return Model::one($query[0], new RecipeData());
    }

    public static function getAll()
    {
        $sql = "select * from " . self::$tablename;
        $query = Executor::doit($sql);
        return Model::many($query[0], new RecipeData());

    }

    public static function getLike($q)
    {
        $sql = "select * from " . self::$tablename . " where name like '%$q%'";
        $query = Executor::doit($sql);
        return Model::many($query[0], new RecipeData());
    }


    public static function getRepeated($pacient_id, $medic_id, $date_at, $time_at)
    {
        $sql = "select * from " . self::$tablename . " where pacient_id=$pacient_id and medic_id=$medic_id and date_at=\"$date_at\" and time_at=\"$time_at\"";
        $query = Executor::doit($sql);
        return Model::one($query[0], new RecipeData());
    }

    public static function getLast($pacient_id, $medic_id)
    {
        $sql = "select max(id) as id from " . self::$tablename . " where pacient_id=$pacient_id and medic_id=$medic_id ";
        $query = Executor::doit($sql);
        return Model::many($query[0], new RecipeData());
    }

    public static function getLastMax($pacient_id, $medic_id)
    {
        $sql = "select *  from " . self::$tablename . " where pacient_id=" . $pacient_id . " and medic_id=" . $medic_id . "  and status_id=1 order by id DESC Limit 1";
        //echo $sql;
        $query = Executor::doit($sql);
        return Model::one($query[0], new RecipeData());
    }


    public static function getBySQL($sql)
    {
        $query = Executor::doit($sql);
        return Model::many($query[0], new RecipeData());
    }

    public static function getBySQLast($sql)
    {
        $query = Executor::doit($sql);
        return Model::one($query[0], new RecipeData());
    }


    public static function getAllPendings()
    {
        $sql = "select * from " . self::$tablename . " where date(date_at)>=date(NOW()) and status_id=1 order by date_at";
        $query = Executor::doit($sql);
        return Model::many($query[0], new RecipeData());
    }

    public static function getAllRecipe($id)
    {
        $sql = "select * from " . self::$tablename . " where historial_id=$id";
        //echo $sql;
        $query = Executor::doit($sql);
        return Model::many($query[0], new RecipeData());
    }
}

?>