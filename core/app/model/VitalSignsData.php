<?php

/**
 * Created by Carlos Gerd CLaros.
 * User: User
 * Date: 11/15/2018
 * Time: 10:36 AM
 */
class VitalSignsData
{
    public static $tablename = "vitalsigns";


    public function VitalSignsData()
    {
        $this->bloodpressureleft = "";
        $this->bloodpressureright = "";
        $this->heartrate = "";
        $this->breathingfrequency = "";
        $this->temperature = "";
        $this->weight = "";
        $this->size = "";
        $this->IMC = "";
        $this->saturation = "";
        $this->created_at = "NOW()";
        $this->date_at = "NOW()";
        $this->time_at = "NOW()";


    }

    public function getPacient()
    {
        return PacientData::getById($this->pacient_id);
    }

    public function getMedic()
    {
        return MedicData::getById($this->medic_id);
    }

    //  public function getStatus(){ return StatusData::getById($this->status_id); }


    public function add()
    {

        $sql = "insert into vitalsigns (bloodpressureleft, bloodpressureright, heartrate, breathingfrequency, temperature, weight, size, IMC,saturation, date_at, time_at, created_at, pacient_id, user_id, medic_id) ";
        $sql .= "value (\"$this->bloodpressureleft\",\"$this->bloodpressureright\",\"$this->heartrate\",\"$this->breathingfrequency\",\"$this->temperature\",\"$this->weight\",\"$this->size\",\"$this->IMC\",\"$this->saturation\",$this->date_at,$this->time_at,$this->created_at,\"$this->pacient_id\",\"$this->user_id\",\"$this->medic_id\")";
        //echo $sql;
        return Executor::doit($sql);
    }

    public static function delById($id)
    {
        $sql = "delete from " . self::$tablename . " where id=$id";
        Executor::doit($sql);
    }

    public function del()
    {
        $sql = "delete from " . self::$tablename . " where id=$this->id";
        Executor::doit($sql);
    }

    public function delupdate()
    {
        $sql = "update " . self::$tablename . " set status_id=0  where id=$this->id";
        //echo $sql ;
        Executor::doit($sql);
    }
// partiendo de que ya tenemos creado un objecto VitalSignsData previamente utilizamos el contexto


    public function update()
    {
        $sql = "update " . self::$tablename . " set bloodpressureleft=\"$this->bloodpressureleft\",bloodpressureright=\"$this->bloodpressureright\",heartrate=\"$this->heartrate\",breathingfrequency=\"$this->breathingfrequency\",temperature=\"$this->temperature\",weight=\"$this->weight\",size=\"$this->size\",IMC=\"$this->IMC\",saturation=\"$this->saturation\" where id=$this->id";
        Executor::doit($sql);
    }

    public static function getById($id)
    {
        $sql = "select * from " . self::$tablename . " where id=$id";
        $query = Executor::doit($sql);
        return Model::one($query[0], new VitalSignsData());
    }

    public static function getAll()
    {
        $sql = "select * from " . self::$tablename;
        $query = Executor::doit($sql);
        return Model::many($query[0], new VitalSignsData());

    }

    public static function getLike($q)
    {
        $sql = "select * from " . self::$tablename . " where name like '%$q%'";
        $query = Executor::doit($sql);
        return Model::many($query[0], new VitalSignsData());
    }


    public static function getRepeated($pacient_id, $medic_id, $date_at, $time_at)
    {
        $sql = "select * from " . self::$tablename . " where pacient_id=$pacient_id and medic_id=$medic_id and date_at=\"$date_at\" and time_at=\"$time_at\"";
        $query = Executor::doit($sql);
        return Model::one($query[0], new VitalSignsData());
    }

    public static function getBySQL($sql)
    {

        $query = Executor::doit($sql);


        return Model::many($query[0], new VitalSignsData());
    }

    public static function getAllPendings()
    {
        $sql = "select * from " . self::$tablename . " where date(date_at)>=date(NOW()) and status_id=1 order by date_at";
        $query = Executor::doit($sql);
        return Model::many($query[0], new VitalSignsData());
    }

    public static function getLastMax($pacient_id, $medic_id)
    {
        $sql = "select *  from " . self::$tablename . " where pacient_id=" . $pacient_id . " and medic_id=" . $medic_id . "  and status_id=1 order by id DESC Limit 1";
        //echo $sql;
        $query = Executor::doit($sql);
        return Model::one($query[0], new VitalSignsData());
    }

    public static function getLastMaxDate($pacient_id, $medic_id, $date_at)
    {
        $sql = "select *  from " . self::$tablename . " where created_at<='" . $date_at . "' and pacient_id=" . $pacient_id . " and medic_id=" . $medic_id . "  and status_id=1 order by id DESC Limit 1";
        //echo $sql;
        $query = Executor::doit($sql);
        return Model::one($query[0], new VitalSignsData());
    }
}

?>