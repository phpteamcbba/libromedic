<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 11/16/2018
 * Time: 3:18 PM
 */

if (count($_POST) > 0) {

    //Function to clean the text data received from post
    function dataready($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    $vital = new HistoryClinicData();
    $vital->pacient_id = $_POST["pacient_id"];
    $vital->medic_id = $_POST["medic_id"];

    $vital->s = html_entity_decode(dataready($_POST["s"]));
    $vital->o = html_entity_decode(dataready($_POST["o"]));
    $vital->i = html_entity_decode(dataready($_POST["i"]));
    $vital->p = html_entity_decode(dataready($_POST["p"]));
    $vital->user_id = $_SESSION["user_id"];
    $vital->add();

    $user = HistoryClinicData::getLast($_POST["pacient_id"], $_POST["medic_id"]);
    //echo $user->id;
    //var_dump

    foreach ($user as $p) {
        $ultimo = $p->id;
    }
    $receta = new RecipeData();
    $receta->pacient_id = $_POST["pacient_id"];
    $receta->medic_id = $_POST["medic_id"];
    $receta->historial_id = $ultimo;
    $receta->recipe = html_entity_decode(dataready($_POST["p"]));
    $receta->recipe_pdf = html_entity_decode(dataready($_POST["p"]));

    $receta->add();

    print "<script>window.location='index.php?view=historiaclinica';</script>";

}


?>