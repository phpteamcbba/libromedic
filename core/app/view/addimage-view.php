<?php
/**
 * BookMedik
 * @author evilnapsis
 * @url http://evilnapsis.com/about/
 **/
include "core/controller/class.upload.php";


if (count($_POST) > 0) {

    $error = false;

    $files = array();
    foreach ($_FILES['image'] as $k => $l) {
        foreach ($l as $i => $v) {
            if (!array_key_exists($i, $files))
                $files[$i] = array();
            $files[$i][$k] = $v;
        }
    }

    foreach ($files as $file) {
        $handle = new Upload($file);
        if ($handle->uploaded) {
            $handle->Process("uploads/");
            if ($handle->processed) {
                // usamos la funcion insert_img de la libreria db.php
                $user = new ImageData();
                $user->folder = "uploads/";
                $user->historial_id = $_POST["historial_id"];
                $user->description = $_POST["description"];
                $user->image = $handle->file_dst_name;
                $user->add();
            } else {
                $error = true;
                echo 'Error: ' . $handle->error;
            }
        } else {
            $error = true;
            echo 'Error: ' . $handle->error;
        }
        unset($handle);
    }

    if (!$error) {
        print "<script>window.location='index.php?view=historiaclinica';</script>";

    }


}


?>