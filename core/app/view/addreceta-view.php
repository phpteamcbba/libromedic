<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 11/16/2018
 * Time: 3:18 PM
 */

if (count($_POST) > 0) {

    function dataready($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        $data = nl2br($data);
        return $data;
    }

    $vital = new RecipeData();
    $vital->pacient_id = $_POST["pacient_id"];
    $vital->medic_id = $_POST["medic_id"];
    $vital->historial_id = $_POST["historial_id"];
    $vital->recipe = html_entity_decode(dataready($_POST["recipe"]));
    $vital->recipe_pdf = html_entity_decode(dataready($_POST["recipe"]));

    $vital->add();


    $user = RecipeData::getLast($_POST["pacient_id"], $_POST["medic_id"]);
    //echo $user->id;
    //var_dump

    foreach ($user as $p) {
        $ultimo = $p->id;
    }

//echo $ultimo;
    $p = $_POST["pacient_id"];
    $m = $_POST["medic_id"];


    $recibo = RecipeData::getById($ultimo);
    $paciente = PacientData::getById($_POST["pacient_id"]);
    $medico = MedicData::getById($_POST["medic_id"]);

# Contenido HTML del documento que queremos generar en PDF.

    $html = '<html lang="en">
<head>
  <meta charset="utf-8">
  <style>
  @page {
    margin: 0;
  }
  *{
    color: #111;
  }
  body {
    font-size: 11pt;
    padding: 0;
    margin: 0;
  }
  .contenedor{
    background-repeat: no-repeat;
    background-position: 0 0;
  }
  .background{
    position: fixed;
    top: 0;
    left: 0;
    width: 813.229pt;
    height: 507.401pt;
    z-index: 1;
  }
  .background img{
    width: 100%;
    height: auto;
  }
  .cuerpo{
    page-break-after: auto;
    max-width: 396.850pt;
    max-height: 606.614pt;
    z-index: 10;
    margin: 113.385pt 28.346pt 28.346pt 56.692pt;
  }
  .footer {
                position: fixed; 
                bottom: -60px; 
                left: 0px; 
                right: 0px;
                height: 50px; 

                /** Extra personal styles **/
                background-color: #03a9f4;
                color: white;
                text-align: center;
                line-height: 35px;
  }
  tr  { page-break-before: auto; max-height: 50pt; }
  td{
    border-bottom: 1px dotted rgba(0,0,0,0.04);
  }
  #duracion, #completado{ display: none; }
  </style>
</head><body> 
 <div class=contenedor>
      <div class=cuerpo>
<table class=table style=border: 0;>
  <tr>
  <td align=right>&nbsp;</td>
  <td><b>' . $paciente->name . " " . $paciente->name . '</b></td>
  </tr>
  <tr>
  <td>&nbsp;</td><td nowrap>' . $recibo->recipe_pdf . '</td>
  </tr>
</table>

</div></div>
<div class=footer>
' . $paciente->name . " " . $paciente->name . '
</div>
';
    $_SESSION["report_recibo"] = $html;
//$categories = CategoryData::getAll();


    //print "<script>window.location='index.php?view=print&id=$ultimo&p=$p&m=$m';</script>";
    //print "<script>window.location='./fpdf/recetapdf.php?id=$ultimo&p=$p&m=$m';</script>";
    //print "<script>window.location='./dompdf061/exmple.php?id=$ultimo&p=$p&m=$m';</script>";
    //print "<script>window.location='index.php?view=print&id=$ultimo&p=$p&m=$m';</script>";
    print "<script>window.location='./dompdf/testexample.php';</script>";
}


?>