<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 11/16/2018
 * Time: 3:18 PM
 */

if (count($_POST) > 0) {
    $vital = new VitalSignsData();
    $vital->pacient_id = $_POST["pacient_id"];
    $vital->medic_id = $_POST["medic_id"];

    $vital->bloodpressureleft = $_POST["bloodpressureleft"];
    $vital->bloodpressureright = $_POST["bloodpressureright"];
    $vital->heartrate = $_POST["heartrate"];

    $vital->breathingfrequency = $_POST["breathingfrequency"];
    $vital->temperature = $_POST["temperature"];
    $vital->weight = $_POST["weight"];

    $vital->user_id = $_SESSION["user_id"];
    $vital->size = $_POST["size"];
    $vital->IMC = $_POST["IMC"];
    $vital->saturation = $_POST["saturation"];

    $vital->add();

    print "<script>window.location='index.php?view=reportssignalvital';</script>";

}


?>