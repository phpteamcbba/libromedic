<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="blue">
                <h4 class="title">Signos Vitales</h4>
            </div>
            <div class="card-content table-responsive">

                <form class="form-horizontal" method="post" id="addproduct" action="index.php?view=addvitalsigns"
                      role="form">
                    <?php
                    $pacients = PacientData::getAll();
                    $medics = MedicData::getAll();

                    ?>

                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Paciente</label>
                        <div class="col-md-6">
                            <select name="pacient_id" class="form-control" id="pacient_id" required>
                                <option value="">PACIENTE</option>
                                <?php foreach ($pacients as $p): ?>
                                    <option value="<?php echo $p->id; ?>" <?php if (isset($_GET["pacient_id"]) && $_GET["pacient_id"] == $p->id) {
                                        echo "selected";
                                    } ?>><?php echo $p->id . " - " . $p->name . " " . $p->lastname; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Medico</label>
                        <div class="col-md-6">
                            <select name="medic_id" class="form-control" id="medic_id">
                                <option value="">MEDICO</option>
                                <?php foreach ($medics as $p): ?>
                                    <option value="<?php echo $p->id; ?>" <?php if (isset($_GET["medic_id"]) && $_GET["medic_id"] == $p->id) {
                                        echo "selected";
                                    } ?>><?php echo $p->id . " - " . $p->name . " " . $p->lastname; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Presion Arterial Mano Izquierda</label>
                        <div class="col-md-6">
                            <textarea name="bloodpressureleft" required class="form-control" id="bloodpressureleft"
                                      placeholder="Presion Arterial"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Presion Arterial Mano Derecha</label>
                        <div class="col-md-6">
                            <textarea name="bloodpressureright" class="form-control" id="bloodpressureright"
                                      placeholder="Presion Arterial"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Frecuencia Cardiaca</label>
                        <div class="col-md-6">
                            <textarea name="heartrate" class="form-control" id="heartrate"
                                      placeholder="Frecuencia Cardiaca"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Frecuencia Respiratoria</label>
                        <div class="col-md-6">
                            <textarea name="breathingfrequency" class="form-control" id="breathingfrequency"
                                      placeholder="Frecuencia Respiratoria"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Temperatura</label>
                        <div class="col-md-6">
                            <textarea name="temperature" class="form-control" id="temperature"
                                      placeholder="Temperatura"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Peso</label>
                        <div class="col-md-6">
                            <textarea name="weight" class="form-control" id="weight"
                                      placeholder="Peso"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Altura</label>
                        <div class="col-md-6">
                            <textarea name="size" class="form-control" id="size"
                                      placeholder="Altura"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">IMC</label>
                        <div class="col-md-6">
                            <textarea name="IMC" class="form-control" id="IMC"
                                      placeholder="Indice de masa corporal"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Saturacion</label>
                        <div class="col-md-6">
                            <textarea name="saturation" class="form-control" id="saturation"
                                      placeholder="Saturacion"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button type="submit" class="btn btn-primary">Agregar Signos Vitales</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>