<?php

if (isset($_GET["id"])) {
    //  include "db.php";
    $img = ImageData::getById($_GET["id"]);
    if ($img != null) {
        // del($img->id);

        $fullpath = $img->folder . $img->src;
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header("Content-Disposition:attachment;filename='" . $img->src . "'");
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        ob_clean();
        flush();
        readfile($fullpath);
        exit();
    }
}


?>