<script src="ckeditor/ckeditor.js"></script>
<?php
$user = HistoryClinicData::getById($_GET["id"]);
//$categories = CategoryData::getAll();
?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="blue">
                <h4 class="title">Editar Historia Clinica</h4>
            </div>
            <div class="card-content table-responsive">

                <form class="form-horizontal" method="post" id="addproduct" action="index.php?view=updatehistoryclinic"
                      role="form">
                    <?php
                    $pacients = PacientData::getAll();
                    $medics = MedicData::getAll();

                    ?>
                    <input type="hidden" name="signal_id" value="<?php echo $_GET["id"]; ?>">
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Paciente</label>
                        <div class="col-md-6">
                            <select name="pacient_id" class="form-control" id="pacient_id" required>

                                <?php foreach ($pacients as $p): ?>
                                    <option value="<?php echo $p->id; ?>" <?php if ($user->id == $p->id) {
                                        echo "selected";
                                    } ?>><?php echo $p->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Medico</label>
                        <div class="col-md-6">
                            <select name="medic_id" class="form-control" id="medic_id">
                                <option value="">MEDICO</option>
                                <?php foreach ($medics as $p): ?>
                                    <option value="<?php echo $p->id; ?>" <?php if ($user->id == $p->id) {
                                        echo "selected";
                                    } ?>><?php echo $p->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">S</label>
                        <div class="col-md-6">
                            <textarea name="s" required class="form-control" id="s"
                                      placeholder="S"><?php echo $user->s; ?></textarea>
                        </div>
                    </div>
                    <script type="text/javascript">
                        CKEDITOR.replace('s');
                    </script>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">O</label>
                        <div class="col-md-6">
                            <textarea name="o" class="form-control" id="o"
                                      placeholder="0"><?php echo $user->o; ?></textarea>
                        </div>
                    </div>
                    <script type="text/javascript">
                        CKEDITOR.replace('o');
                    </script>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">I</label>
                        <div class="col-md-6">
                            <textarea name="i" class="form-control" id="i"
                                      placeholder="Frecuencia Cardiaca"><?php echo $user->i; ?></textarea>
                        </div>
                    </div>
                    <script type="text/javascript">
                        CKEDITOR.replace('i');
                    </script>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">P</label>
                        <div class="col-md-6">
                            <textarea name="p" class="form-control" id="p"
                                      placeholder="p"><?php echo $user->p; ?></textarea>
                        </div>
                    </div>
                    <script type="text/javascript">
                        CKEDITOR.replace('p');
                    </script>

                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button type="submit" class="btn btn-primary">Modificar Historial Clinico</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>