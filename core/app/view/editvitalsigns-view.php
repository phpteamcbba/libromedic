<?php
$user = VitalSignsData::getById($_GET["id"]);
//$categories = CategoryData::getAll();
?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="blue">
                <h4 class="title">Editar Signos Vitales</h4>
            </div>
            <div class="card-content table-responsive">

                <form class="form-horizontal" method="post" id="addproduct" action="index.php?view=updatesignalvital"
                      role="form">
                    <?php
                    $pacients = PacientData::getAll();
                    $medics = MedicData::getAll();

                    ?>
                    <input type="hidden" name="signal_id" value="<?php echo $_GET["id"]; ?>">
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Paciente</label>
                        <div class="col-md-6">
                            <select name="pacient_id" class="form-control" id="pacient_id" required>

                                <?php foreach ($pacients as $p): ?>
                                    <option value="<?php echo $p->id; ?>" <?php if ($user->id == $p->id) {
                                        echo "selected";
                                    } ?>><?php echo $p->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Medico</label>
                        <div class="col-md-6">
                            <select name="medic_id" class="form-control" id="medic_id">
                                <option value="">MEDICO</option>
                                <?php foreach ($medics as $p): ?>
                                    <option value="<?php echo $p->id; ?>" <?php if ($user->id == $p->id) {
                                        echo "selected";
                                    } ?>><?php echo $p->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Presion Arterial Mano Izquierda</label>
                        <div class="col-md-6">
                            <textarea name="bloodpressureleft" required class="form-control" id="bloodpressureleft"
                                      placeholder="Presion Arterial"><?php echo $user->bloodpressureleft; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Presion Arterial Mano Derecha</label>
                        <div class="col-md-6">
                            <textarea name="bloodpressureright" class="form-control" id="bloodpressureright"
                                      placeholder="Presion Arterial"><?php echo $user->bloodpressureright; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Frecuencia Cardiaca</label>
                        <div class="col-md-6">
                            <textarea name="heartrate" class="form-control" id="heartrate"
                                      placeholder="Frecuencia Cardiaca"><?php echo $user->heartrate; ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Frecuencia Respiratoria</label>
                        <div class="col-md-6">
                            <textarea name="breathingfrequency" class="form-control" id="breathingfrequency"
                                      placeholder="Frecuencia Respiratoria"><?php echo $user->breathingfrequency; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Temperatura</label>
                        <div class="col-md-6">
                            <textarea name="temperature" class="form-control" id="temperature"
                                      placeholder="Temperatura"><?php echo $user->temperature; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Peso</label>
                        <div class="col-md-6">
                            <textarea name="weight" class="form-control" id="weight"
                                      placeholder="Peso"><?php echo $user->weight; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Altura</label>
                        <div class="col-md-6">
                            <textarea name="size" class="form-control" id="size"
                                      placeholder="Altura"><?php echo $user->size; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">IMC</label>
                        <div class="col-md-6">
                            <textarea name="IMC" class="form-control" id="IMC"
                                      placeholder="Indice de masa corporal"><?php echo $user->IMC; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Saturacion</label>
                        <div class="col-md-6">
                            <textarea name="saturation" class="form-control" id="saturation"
                                      placeholder="Saturacion"><?php echo $user->saturation; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button type="submit" class="btn btn-primary">Modificar Signos Vitales</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>