<!-- make sure the src path points to your copied ckeditor folder -->
<script src="ckeditor/ckeditor.js"></script>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="blue">
                <h4 class="title">Historia Clinica</h4>
            </div>
            <div class="card-content table-responsive">

                <form class="form-horizontal" method="post" id="addproduct" action="index.php?view=addhistorialclinic"
                      role="form">
                    <?php
                    if ((isset($_GET["id"]))) {
                        $pacients = PacientData::getAll();
                        $paciente = PacientData::getById($_GET["id"]);
                        $signalvital = VitalSignsData::getBySQL("SELECT * FROM vitalsigns WHERE pacient_id=" . $_GET["id"] . "  order by id desc Limit 1");
                    } else {
                        $pacients = PacientData::getAll();

                    }



                    $medics = MedicData::getAll();


                    if ((isset($_GET["id"])) && ($_GET["id"] != "")) {
                        ?>
                        <div class="form-group">
                            <label for="inputEmail1" class="col-lg-2 control-label">Paciente</label>
                            <input type="hidden" name="pacient_id" id="pacient_id" value="<?php echo $_GET["id"]; ?>">
                            <div class="col-md-6">
                                <table class="table table-bordered">
                                    <thead>
                                    <th>Pacientes</th>
                                    <th>PA Lefth</th>
                                    <th>PA right</th>
                                    <th>FC</th>
                                    <th>FR</th>
                                    <th>temperatura</th>
                                    <th>Peso</th>
                                    <th>Tama</th>
                                    <th>Indice Masa Corporal</th>

                                    </thead>
                                    <?php

                                    foreach ($signalvital as $user) {
                                        $pacientes = $user->getPacient();

                                        ?>
                                        <tr>
                                            <td><?php echo $pacientes->name . " " . $pacientes->lastname; ?></td>
                                            <td><?php echo $user->bloodpressureleft; ?></td>
                                            <td><?php echo $user->bloodpressureright; ?></td>
                                            <td><?php echo $user->heartrate; ?></td>
                                            <td><?php echo $user->breathingfrequency; ?></td>
                                            <td><?php echo $user->temperature; ?></td>
                                            <td><?php echo $user->weight; ?></td>
                                            <td><?php echo $user->size; ?></td>
                                            <td><?php echo $user->IMC; ?></td>

                                        </tr>
                                        <?php
                                    }
                                    echo "</table>";
                                    ?>

                            </div>
                        </div>

                    <?php } else { ?>

                        <div class="form-group">
                            <label for="inputEmail1" class="col-lg-2 control-label">Paciente</label>
                            <div class="col-md-6">
                                <select name="pacient_id" class="form-control" id="pacient_id" required>
                                    <option value="">PACIENTE</option>
                                    <?php foreach ($pacients as $p): ?>
                                        <option value="<?php echo $p->id; ?>" <?php if (isset($_GET["pacient_id"]) && $_GET["pacient_id"] && $_GET["id"] && (isset($_GET["id"])) == $p->id) {
                                            echo "selected";
                                        } ?>><?php echo $p->id . " - " . $p->name . " " . $p->lastname; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>


                    <?php } ?>

                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Medico</label>
                        <div class="col-md-6">
                            <select name="medic_id" class="form-control" id="medic_id" required>
                                <option value="">MEDICO</option>
                                <?php foreach ($medics as $p): ?>
                                    <option value="<?php echo $p->id; ?>" <?php if (isset($_GET["medic_id"]) && $_GET["medic_id"] == $p->id) {
                                        echo "selected";
                                    } ?>><?php echo $p->id . " - " . $p->name . " " . $p->lastname; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label"><font size="40">S</font></label>
                        <div class="col-lg-5">
                            <textarea name="s" required class="form-control" id="s"
                                      placeholder="s"></textarea>
                        </div>
                    </div>
                    <script type="text/javascript">
                        CKEDITOR.replace('s', {
                            toolbar: [
                                {name: 'basicstyles', items: ['Bold', 'Italic']},
                                {
                                    name: 'paragraph',
                                    groups: ['list', 'indent', 'blocks', 'align', 'bidi'],
                                    items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']
                                }
                            ]
                        });
                    </script>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label"><font size="40">O</font></label>
                        <div class="col-md-5">
                            <textarea name="o" class="form-control" id="o"
                                      placeholder="O"></textarea>
                        </div>
                    </div>
                    <script type="text/javascript">
                        CKEDITOR.replace('o', {
                            toolbar: [
                                {name: 'basicstyles', items: ['Bold', 'Italic']},
                                {
                                    name: 'paragraph',
                                    groups: ['list', 'indent', 'blocks', 'align', 'bidi'],
                                    items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']
                                }
                            ]
                        });
                    </script>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label"><font size="40">I</font></label>
                        <div class="col-md-5">
                            <textarea name="i" class="form-control" id="i"
                                      placeholder="I"></textarea>
                        </div>
                    </div>
                    <script type="text/javascript">
                        CKEDITOR.replace('i', {
                            toolbar: [
                                {name: 'basicstyles', items: ['Bold', 'Italic']},
                                {
                                    name: 'paragraph',
                                    groups: ['list', 'indent', 'blocks', 'align', 'bidi'],
                                    items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']
                                }
                            ]
                        });
                    </script>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label"><font size="40">P</font></label>
                        <div class="col-md-5">
                            <textarea name="p" class="form-control" id="p"
                                      placeholder="P"></textarea>
                        </div>
                    </div>

                    <script type="text/javascript">
                        CKEDITOR.replace('p', {
                            toolbar: [
                                // Line break - next group will be placed in new line.
                                {name: 'basicstyles', items: ['Bold', 'Italic']},
                                {
                                    name: 'paragraph',
                                    groups: ['list', 'indent', 'blocks', 'align', 'bidi'],
                                    items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']
                                }
                            ]
                        });
                    </script>
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button type="submit" class="btn btn-primary">Agregar Historia Clinica</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>