<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="blue">
                <h4 class="title">Imagenes de Historial Clinico</h4>
            </div>
            <div class="card-content table-responsive">


                <form class="form-horizontal" role="form">
                    <input type="hidden" name="view" value="historiaclinica">
                    <?php
                    $pacients = PacientData::getAll();
                    $medics = MedicData::getAll();
                    $statuses = StatusData::getAll();
                    $vitalsignsdata = VitalSignsData::getAll();
                    $historyclinic = HistoryClinicData::getAll();
                    $images = ImageData::getByIdHistorial($_GET["id"])
                    ?>

                    <div class="form-group">

                        <div class="col-lg-3">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-male"></i></span>
                                <select name="pacient_id" class="form-control">
                                    <option value="">PACIENTE</option>
                                    <?php foreach ($pacients as $p): ?>
                                        <option value="<?php echo $p->id; ?>" <?php if (isset($_GET["pacient_id"]) && $_GET["pacient_id"] == $p->id) {
                                            echo "selected";
                                        } ?>><?php echo $p->id . " - " . $p->name . " " . $p->lastname; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>


                    </div>
                    <div class="form-group">


                        <div class="col-lg-6">
                            <!--<button class="btn btn-primary btn-block">Procesar</button>-->
                        </div>

                    </div>
                </form>

                <?php
                $users = array();
                if ((isset($_GET["pacient_id"]) && isset($_GET["id"])) && ($_GET["pacient_id"] != "" || $_GET["id"] != "")) {
                    $sql = "select * from  image where ";


                    if ($_GET["id"] != "") {


                        $sql .= " historial_id = " . $_GET["id"];
                    }


                    $sql .= "  order by  created_at DESC";
                    // echo $sql;
                    $users = ImageData::getBySQL($sql);

                } else {
                    $users = ImageData::getAll();

                }
                if (count($users) > 0){
                // si hay usuarios
                //$_SESSION["report_history_clinic"] = $users;
                ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Reportes
                    </div>
                    <table class="table table-bordered table-hover">
                        <thead>
                        <th>Folder</th>
                        <th>Image</th>
                        <th></th>

                        </thead>
                        <?php
                        $total = 0;
                        foreach ($users as $user) {

                            ?>
                            <tr>
                                <td><?php echo $user->folder; ?></td>
                                <td><img src="<?php echo $user->folder . $user->src; ?>" style="width:240px;"></td>

                                <td style="width:280px;">


                                    <a href="index.php?view=downloadimages&id=<?php echo $user->id; ?>"
                                       class="btn btn-info btn-group-xs">Download</a>

                                </td>
                            </tr>
                            <?php
                            // $total += $user->price;

                        }
                        echo "</table>";
                        ?>



                        <?php
                        } else {

                            echo "<p class='alert alert-danger'>No hay imagenes</p>";
                        }

                        ?>
                </div>
            </div>

        </div>
    </div>
