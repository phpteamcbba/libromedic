<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="blue">
                <h4 class="title">Reporte de Recetas</h4>
            </div>
            <div class="card-content table-responsive">


                <form class="form-horizontal" role="form">
                    <input type="hidden" name="view" value="listrecipe">
                    <input type="hidden" name="id" value="<?php echo $_GET["id"]; ?>">
                    <input type="hidden" name="pacient_id" value="<?php echo $_GET["pacient_id"]; ?>">
                    <input type="hidden" name="medic_id" value="<?php echo $_GET["medic_id"]; ?>">
                    <?php
                    $pacients = PacientData::getAll();
                    $medics = MedicData::getAll();
                    $recipe = RecipeData::getAll();
                    ?>
                    <div class="form-group">
                        <div class="col-lg-3">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-male"></i></span>
                                <p>PACIENTE</p>
                                <?php foreach ($pacients as $p): ?>
                                    <p><?php if (isset($_GET["pacient_id"]) && $_GET["pacient_id"] == $p->id) {
                                            echo "";
                                            echo $p->id . " - " . $p->name . " " . $p->lastname;
                                        } ?></p>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-support"></i></span>
                                <p>MEDICO</p>
                                <?php foreach ($medics as $p): ?>
                                    <p><?php if (isset($_GET["medic_id"]) && $_GET["medic_id"] == $p->id) {
                                            echo "";
                                            echo $p->id . " - " . $p->name . " " . $p->lastname;
                                        } ?></p>
                                <?php endforeach; ?>

                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="input-group">
                                <span class="input-group-addon">INICIO</span>
                                <input type="date" name="start_at"
                                       value="<?php if (isset($_GET["start_at"]) && $_GET["start_at"] != "") {
                                           echo $_GET["start_at"];
                                       } ?>" class="form-control" placeholder="Palabra clave">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="input-group">
                                <span class="input-group-addon">FIN</span>
                                <input type="date" name="finish_at"
                                       value="<?php if (isset($_GET["finish_at"]) && $_GET["finish_at"] != "") {
                                           echo $_GET["finish_at"];
                                       } ?>" class="form-control" placeholder="Palabra clave">
                            </div>
                        </div>

                    </div>
                    <div class="form-group">


                        <div class="col-lg-6">
                            <button class="btn btn-primary btn-block">Procesar</button>
                        </div>

                    </div>
                </form>

                <?php
                $users = array();
                if ((isset($_GET["id"]) && isset($_GET["pacient_id"]) && isset($_GET["medic_id"]) && isset($_GET["start_at"]) && isset($_GET["finish_at"])) && ($_GET["id"] != "" || $_GET["pacient_id"] != "" || $_GET["medic_id"] != "" || ($_GET["start_at"] != "" && $_GET["finish_at"] != ""))) {
                    $sql = "select * from recipe where  ";
                    if ($_GET["id"] != "") {
                        $sql .= " id = " . $_GET["id"];
                    }

                    if ($_GET["pacient_id"] != "") {
                        if ($_GET["id"] != "") {
                            $sql .= " and ";
                        }
                        $sql .= " pacient_id = " . $_GET["pacient_id"];
                    }

                    if ($_GET["medic_id"] != "") {
                        $sql .= " and medic_id = " . $_GET["medic_id"];
                    }

                    if ($_GET["start_at"] != "" && $_GET["finish_at"]) {
                        if ($_GET["id"] != "" || $_GET["pacient_id"] != "" || $_GET["medic_id"] != "") {
                            $sql .= " and ";
                        }

                        $sql .= " ( date_at >= \"" . $_GET["start_at"] . "\" and date_at <= \"" . $_GET["finish_at"] . "\" ) ";
                    }

//echo $sql;
                    $users = RecipeData::getBySQL($sql);

                } else {
                    $users = RecipeData::getAllRecipe($_GET["id"]);

                }
                if (count($users) > 0){
                // si hay usuarios
                $_SESSION["report_data"] = $users;
                ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Reportes
                    </div>
                    <table class="table table-bordered table-hover">
                        <thead>
                        <th>Paciente</th>
                        <th>Medico</th>
                        <th>Recipe</th>

                        </thead>
                        <?php
                        $total = 0;
                        foreach ($users as $user) {
                            $pacient = $user->getPacient();
                            $medic = $user->getMedic();
                            ?>
                            <tr>
                                <td><?php echo $pacient->name . " " . $pacient->lastname; ?></td>
                                <td><?php echo $medic->name . " " . $medic->lastname; ?></td>
                                <td><?php echo $user->recipe; ?></td>
                                <td style="width:280px;">

                                    <!-- <a href="index.php?view=edithistoryclinic&id=<?php echo $user->id; ?>"
                                       class="btn btn-warning btn-group-xs">Editar</a>
                                    <a href="index.php?view=delhistoryclinic&id=<?php echo $user->id; ?>"
                                       class="btn btn-danger btn-group-xs">Eliminar</a> -->
                                    <a href="./dompdf/imprimirreceta.php?id=<?php echo $user->id; ?>&pacient_id=<?php echo $pacient->id; ?>&medic_id=<?php echo $medic->id; ?>"
                                       class="btn btn-info btn-xs">Imprimir</a>
                                </td>


                            </tr>
                            <?php

                        }
                        echo "</table>";
                        ?>
                        <?php
                        } else {
                            echo "<p class='alert alert-danger'>No hay pacientes</p>";
                        }
                        ?>

                </div>
            </div>

        </div>
    </div>
