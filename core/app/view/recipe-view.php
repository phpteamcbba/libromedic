<!-- make sure the src path points to your copied ckeditor folder -->

<script src="ckeditor/ckeditor.js"></script>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="blue">
                <h4 class="title">Receta</h4>
            </div>
            <div class="card-content table-responsive">

                <form class="form-horizontal" method="post" id="addproduct" action="index.php?view=addreceta"
                      role="form">
                    <?php
                    $pacients = PacientData::getAll();
                    $medics = MedicData::getAll();
                    $signalvital = VitalSignsData::getAll();
                    $recipe = RecipeData::getAll();


                    ?>
                    <input type="hidden" name="historial_id" value="<?php echo $_GET["id"]; ?>">
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Paciente</label>
                        <div class="col-md-6">
                            <select name="pacient_id" class="form-control" id="pacient_id" required>
                                <option value="">PACIENTE</option>
                                <?php foreach ($pacients as $p): ?>
                                    <option value="<?php echo $p->id; ?>" <?php if (isset($_GET["pacient_id"]) && $_GET["pacient_id"] == $p->id) {
                                        echo "selected";
                                    } ?>><?php echo $p->id . " - " . $p->name . " " . $p->lastname; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Medico</label>
                        <div class="col-md-6">
                            <select name="medic_id" class="form-control" id="medic_id">
                                <option value="">MEDICO</option>
                                <?php foreach ($medics as $p): ?>
                                    <option value="<?php echo $p->id; ?>" <?php if (isset($_GET["medic_id"]) && $_GET["medic_id"] == $p->id) {
                                        echo "selected";
                                    } ?>><?php echo $p->id . " - " . $p->name . " " . $p->lastname; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">RECETA</label>
                        <div class="col-md-6">
                            <textarea cols="1" rows="1" name="recipe" required class="form-control" id="recipe"
                                      placeholder="RECETA"></textarea>
                        </div>
                    </div>
                    <script type="text/javascript">
                        CKEDITOR.replace('recipe');
                    </script>

                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button type="submit" class="btn btn-primary">Agregar Receta</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>