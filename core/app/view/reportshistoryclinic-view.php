<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="blue">
                <h4 class="title">Reporte de Historial Clinico</h4>
            </div>
            <div class="card-content table-responsive">


                <form class="form-horizontal" role="form">
                    <input type="hidden" name="view" value="reportshistoryclinic">
                    <?php
                    $pacients = PacientData::getAll();
                    $medics = MedicData::getAll();
                    $statuses = StatusData::getAll();
                    $vitalsignsdata = VitalSignsData::getAll();
                    $historyclinic = HistoryClinicData::getAll();
                    ?>

                    <div class="form-group">

                        <div class="col-lg-3">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-male"></i></span>
                                <select name="pacient_id" class="form-control">
                                    <option value="">PACIENTE</option>
                                    <?php foreach ($pacients as $p): ?>
                                        <option value="<?php echo $p->id; ?>" <?php if (isset($_GET["pacient_id"]) && $_GET["pacient_id"] == $p->id) {
                                            echo "selected";
                                        } ?>><?php echo $p->id . " - " . $p->name . " " . $p->lastname; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-support"></i></span>
                                <select name="medic_id" class="form-control">
                                    <option value="">MEDICO</option>
                                    <?php foreach ($medics as $p): ?>
                                        <option value="<?php echo $p->id; ?>" <?php if (isset($_GET["medic_id"]) && $_GET["medic_id"] == $p->id) {
                                            echo "selected";
                                        } ?>><?php echo $p->id . " - " . $p->name . " " . $p->lastname; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="input-group">
                                <span class="input-group-addon">INICIO</span>
                                <input type="date" name="start_at"
                                       value="<?php if (isset($_GET["start_at"]) && $_GET["start_at"] != "") {
                                           echo $_GET["start_at"];
                                       } ?>" class="form-control" placeholder="Palabra clave">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="input-group">
                                <span class="input-group-addon">FIN</span>
                                <input type="date" name="finish_at"
                                       value="<?php if (isset($_GET["finish_at"]) && $_GET["finish_at"] != "") {
                                           echo $_GET["finish_at"];
                                       } ?>" class="form-control" placeholder="Palabra clave">
                            </div>
                        </div>

                    </div>
                    <div class="form-group">



                        <div class="col-lg-6">
                            <button class="btn btn-primary btn-block">Procesar</button>
                        </div>

                    </div>
                </form>

                <?php
                $users = array();
                if ((isset($_GET["pacient_id"]) && isset($_GET["medic_id"]) && isset($_GET["start_at"]) && isset($_GET["finish_at"])) && ($_GET["pacient_id"] != "" || $_GET["medic_id"] != "" || ($_GET["start_at"] != "" && $_GET["finish_at"] != ""))) {
                    $sql = "select * from  historialclinic where ";

                    if ($_GET["pacient_id"] != "") {

                        $sql .= " pacient_id = " . $_GET["pacient_id"];
                    }

                    if ($_GET["medic_id"] != "") {
                        if ($_GET["pacient_id"] != "") {
                            $sql .= " and ";
                        }

                        $sql .= " medic_id = " . $_GET["medic_id"];
                    }


                    if ($_GET["start_at"] != "" && $_GET["finish_at"]) {
                        if ($_GET["pacient_id"] != "" || $_GET["medic_id"] != "") {
                            $sql .= " and ";
                        }

                        $sql .= " ( date_at >= \"" . $_GET["start_at"] . "\" and date_at <= \"" . $_GET["finish_at"] . "\" ) ";
                    }
                    $sql .= " and status_id=1 order by  created_at DESC";
                    //    echo $sql;
                    $users = HistoryClinicData::getBySQL($sql);

                } else {
                    $users = HistoryClinicData::getAllPendings();

                }
                if (count($users) > 0){
                // si hay usuarios
                $_SESSION["report_history_clinic"] = $users;
                ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Reportes
                    </div>
                    <table class="table table-bordered table-hover">
                        <thead>
                        <th>Paciente</th>
                        <th>S</th>
                        <th>O</th>
                        <th>I</th>
                        <th>P</th>


                        </thead>
                        <?php
                        $total = 0;
                        foreach ($users as $user) {
                            $pacient = $user->getPacient();
                            $medic = $user->getMedic();
                            ?>
                            <tr>
                                <td><?php echo $pacient->name . " " . $pacient->lastname; ?></td>

                                <td><?php echo $user->s; ?></td>
                                <td><?php echo $user->o; ?></td>
                                <td><?php echo $user->i; ?></td>
                                <td><?php echo $user->p; ?></td>

                            </tr>
                            <?php
                            // $total += $user->price;

                        }
                        echo "</table>";
                        ?>


                        <div class="panel-body">

                            <a href="./fpdf/signalvitalpdf.php" class="btn btn-default" target="_blank"><i
                                        class="fa fa-download"> Descargar (.pdf)</i></a>


                        </div>
                        <?php
                        } else {
                            echo "<p class='alert alert-danger'>No hay pacientes</p>";
                        }

                        ?>
                </div>
            </div>

        </div>
    </div>
