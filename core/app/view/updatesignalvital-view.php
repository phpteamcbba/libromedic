<?php

if (count($_POST) > 0) {
    $vital = VitalSignsData::getById($_POST["signal_id"]);
    $vital->pacient_id = $_POST["pacient_id"];
    $vital->medic_id = $_POST["medic_id"];

    $vital->bloodpressureleft = $_POST["bloodpressureleft"];
    $vital->bloodpressureright = $_POST["bloodpressureright"];
    $vital->heartrate = $_POST["heartrate"];

    $vital->breathingfrequency = $_POST["breathingfrequency"];
    $vital->temperature = $_POST["temperature"];
    $vital->weight = $_POST["weight"];

    $vital->user_id = $_SESSION["user_id"];
    $vital->size = $_POST["size"];
    $vital->IMC = $_POST["IMC"];
    $vital->saturation = $_POST["saturation"];
    $vital->update();
    Core::alert("Actualizado exitosamente!");
    print "<script>window.location='index.php?view=reportssignalvital';</script>";
}


?>