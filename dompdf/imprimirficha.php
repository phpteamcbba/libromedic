<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 11/28/2018
 * Time: 5:31 PM
 */
session_start();

require_once 'lib/html5lib/Parser.php';
require_once 'lib/php-font-lib/src/FontLib/Autoloader.php';
require_once 'lib/php-svg-lib/src/autoload.php';
require_once 'src/Autoloader.php';


require_once '../core/app/model/HistoryClinicData.php';
require_once '../core/app/model/PacientData.php';
require_once '../core/app/model/MedicData.php';
require_once '../core/app/model/RecipeData.php';
require_once '../core/app/model/VitalSignsData.php';


require_once '../core/controller/Executor.php';
require_once '../core/controller/Database.php';
require_once '../core/controller/Core.php';
require_once '../core/controller/Model.php';


$pacient = PacientData::getById($_GET["pacient_id"]);
$medic = MedicData::getById($_GET["medic_id"]);
$recibo = HistoryClinicData::getById($_GET["id"]);
$recipe = RecipeData::getLastMax($_GET["pacient_id"], $_GET["medic_id"]);
$signal = VitalSignsData::getLastMax($_GET["pacient_id"], $_GET["medic_id"]);

//echo $_GET["id"];
//echo $_GET["pacient_id"];
//echo $_GET["medic_id"];

//echo $pacient->name . " " . $pacient->lastname;

$html_recipe = '<html lang="en">
<head>
  <meta charset="utf-8">
  <style>
  @page {
    margin: 0;
  }
  *{
    color: #111;
  }
  body {
    font-size: 11pt;
    padding: 0;
    margin: 0;
  }
  .contenedor{
    background-repeat: no-repeat;
    background-position: 0 0;
  }
  .background{
    position: fixed;
    top: 0;
    left: 0;
    width: 813.229pt;
    height: 507.401pt;
    z-index: 1;
  }
  .background img{
    width: 100%;
    height: auto;
  }
  .cuerpo{
    page-break-after: auto;
    max-width: 396.850pt;
    max-height: 606.614pt;
    z-index: 10;
    margin: 85.0393pt 28.346pt 28.346pt 56.692pt;
  }

  tr  { page-break-before: auto; max-height: 50pt; }
  td{
    border-bottom: 1px dotted rgba(0,0,0,0.04);
  }
  #duracion, #completado{ display: none; }
  </style>
</head><body> 
 <div class=contenedor>
      <div class=cuerpo>


';


function busca_edad($fecha_nacimiento)
{
    $dia = date("d");
    $mes = date("m");
    $ano = date("Y");


    $dianaz = date("d", strtotime($fecha_nacimiento));
    $mesnaz = date("m", strtotime($fecha_nacimiento));
    $anonaz = date("Y", strtotime($fecha_nacimiento));


//si el mes es el mismo pero el día inferior aun no ha cumplido años, le quitaremos un año al actual

    if (($mesnaz == $mes) && ($dianaz > $dia)) {
        $ano = ($ano - 1);
    }

//si el mes es superior al actual tampoco habrá cumplido años, por eso le quitamos un año al actual

    if ($mesnaz > $mes) {
        $ano = ($ano - 1);
    }

    //ya no habría mas condiciones, ahora simplemente restamos los años y mostramos el resultado como su edad

    $edad = ($ano - $anonaz);


    return $edad;


}

$nacimiento = busca_edad($pacient->day_of_birth);
$dias = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

$fecha_actual = date('d') . " de " . $meses[date('n') - 1] . " del " . date('Y');
$html_personal = '<table cellspacing="0" cellpadding="0" style="width:100%" >
<tr>
            <td>&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td><B>No</B></td>
            <td><b>' . $recibo->id . '</b></td>
 </tr>
 <tr>
            <td>&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
 </tr>
 <tr >
            <td>Nombre</td>
            <td><b>' . $pacient->name . " " . $pacient->name . '</b></font> </td>
            <td>Edad</td>
            <td><b>' . $nacimiento . '</b></td>
            <td>Fecha</td>
            <td><b>' . $fecha_actual . '</b></td>
 </tr>
 <tr >
            <td>Estado Civil</td>
            <td><b>' . $pacient->sick . '</b></td>
            <td>Ocupacion</td>
            <td><b>' . $pacient->medicaments . '</b></td>
            <td></td>
            <td></td>
 </tr>
  <tr >
            <td>&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
 </tr>
</table>';


$html_signal = '<table cellspacing="0" cellpadding="0" style="width:100%" >
<tr>
          
            <td><b>PA</b></td>
            <td>' . $signal->bloodpressureright . '</td>
            <td><b>FC</b></td>
            <td>' . $signal->heartrate . '</td>
            <td><b>FR</b></td>
            <td>' . $signal->breathingfrequency . '</td>
            <td><b>To</td>
            <td>' . $signal->temperature . '</td>
            <td><b>Peso</b></td>
            <td>' . $signal->weight . '</td>
            <td><b>Talla</b></td>
            <td>' . $signal->size . '</td>
            <td><b>Saturacion</b></td>
            <td>' . $signal->saturation . '</td>
 </tr>
 <tr>
          
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
 </tr>
 </table>';

$html_ficha = '<table cellspacing="0" cellpadding="0" style="width:100%" >';
$html_ficha .= '<tr><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td><h1>S</h1></td><td>' . $recibo->s . '</td><td>';
$html_ficha .= '<tr><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td><h1>O</h1></td><td>' . $recibo->o . '</td><td>';
$html_ficha .= '<tr><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td><h1>I</h1></td><td>' . $recibo->i . '</td><td>';
$html_ficha .= '<tr><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td><h1>P</h1></td><td>' . $recibo->p . '</td><td>';
$html_ficha .= '</table>';

$html_footer = '</div></div></body></html>';

$html = $html_personal . $html_signal . $html_ficha . $html_footer;


Dompdf\Autoloader::register();

// reference the Dompdf namespace
use Dompdf\Dompdf;

// instantiate and use the dompdf class
$dompdf = new Dompdf();


$dompdf->loadHtml(utf8_decode($html));

// (Optional) Setup the paper size and orientation
//$dompdf->setPaper('A4', 'landscape');
//$customPaper = array(0,0,606.614,793.700);
$dompdf->setPaper('legal', 'portrait');
// Render the HTML as PDF
$dompdf->render();


// Output the generated PDF to Browser
$dompdf->stream();

//echo $html;
?>