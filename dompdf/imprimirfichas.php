<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 11/28/2018
 * Time: 5:31 PM
 */
session_start();

require_once 'lib/html5lib/Parser.php';
require_once 'lib/php-font-lib/src/FontLib/Autoloader.php';
require_once 'lib/php-svg-lib/src/autoload.php';
require_once 'src/Autoloader.php';


require_once '../core/app/model/HistoryClinicData.php';
require_once '../core/app/model/PacientData.php';
require_once '../core/app/model/MedicData.php';
require_once '../core/app/model/RecipeData.php';
require_once '../core/app/model/VitalSignsData.php';


require_once '../core/controller/Executor.php';
require_once '../core/controller/Database.php';
require_once '../core/controller/Core.php';
require_once '../core/controller/Model.php';


$pacient = PacientData::getById($_GET["pacient_id"]);
$medic = MedicData::getById($_GET["medic_id"]);
$recipe = RecipeData::getLastMax($_GET["pacient_id"], $_GET["medic_id"]);
//$signal = VitalSignsData::getLastMaxDate($_GET["pacient_id"], $_GET["medic_id"]);



function dataready($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

$users = array();
if ((isset($_GET["pacient_id"]) && isset($_GET["medic_id"]) && isset($_GET["start_at"]) && isset($_GET["finish_at"])) && ($_GET["pacient_id"] != "" || $_GET["medic_id"] != "" || ($_GET["start_at"] != "" && $_GET["finish_at"] != ""))) {
    $sql = "select * from  historialclinic where ";

    if ($_GET["pacient_id"] != "") {

        $sql .= " pacient_id = " . $_GET["pacient_id"];
    }

    if ($_GET["medic_id"] != "") {
        if ($_GET["pacient_id"] != "") {
            $sql .= " and ";
        }

        $sql .= " medic_id = " . $_GET["medic_id"];
    }


    if ($_GET["start_at"] != "" && $_GET["finish_at"]) {
        if ($_GET["pacient_id"] != "" || $_GET["medic_id"] != "") {
            $sql .= " and ";
        }

        $sql .= " ( DATE(date_at) >= \"" . $_GET["start_at"] . "\" and DATE(date_at) <= \"" . $_GET["finish_at"] . "\" ) ";
    }
    $sql .= " and status_id=1 order by  created_at DESC";
    //    echo $sql;
    $users = HistoryClinicData::getBySQL($sql);

} else {
    $users = HistoryClinicData::getAllPendings();

}



$html = '<html lang="en">
<head>
  <meta charset="utf-8">
  <title>A4</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">
  <!-- Load paper.css for happy printing -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">
  <!-- Set page size here: A5, A4 or A3 -->
  <!-- Set also "landscape" if you need -->
  <style>@page { size: legal }</style>
  <!-- <style>
    body   { font-family: serif }
    h1     { font-family: \'Tangerine\', cursive; font-size: 40pt; line-height: 18mm}
    h2, h3 { font-family: \'Tangerine\', cursive; font-size: 24pt; line-height: 7mm }
    h4     { font-size: 32pt; line-height: 14mm }
    h2 + p { font-size: 18pt; line-height: 7mm }
    h3 + p { font-size: 14pt; line-height: 7mm }
    li     { font-size: 11pt; line-height: 5mm }
    h1      { margin: 0 }
    h1 + ul { margin: 2mm 0 5mm }
    h2, h3  { margin: 0 3mm 3mm 0; float: left }
    h2 + p,
    h3 + p  { margin: 0 0 3mm 50mm }
    h4      { margin: 2mm 0 0 50mm; border-bottom: 2px solid black }
    h4 + ul { margin: 5mm 0 0 50mm }
    article { border: 4px double black; padding: 5mm 10mm; border-radius: 3mm }
  </style>-->
</head><body class="legal">';

function busca_edad($fecha_nacimiento)
{
    $dia = date("d");
    $mes = date("m");
    $ano = date("Y");


    $dianaz = date("d", strtotime($fecha_nacimiento));
    $mesnaz = date("m", strtotime($fecha_nacimiento));
    $anonaz = date("Y", strtotime($fecha_nacimiento));


//si el mes es el mismo pero el día inferior aun no ha cumplido años, le quitaremos un año al actual

    if (($mesnaz == $mes) && ($dianaz > $dia)) {
        $ano = ($ano - 1);
    }

//si el mes es superior al actual tampoco habrá cumplido años, por eso le quitamos un año al actual

    if ($mesnaz > $mes) {
        $ano = ($ano - 1);
    }

    //ya no habría mas condiciones, ahora simplemente restamos los años y mostramos el resultado como su edad

    $edad = ($ano - $anonaz);


    return $edad;


}

$dias = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");


$html_header = '';
$html_cuerpo = "";
foreach ($users as $user) {
    $pacient = $user->getPacient();
    $medic = $user->getMedic();
    //paciente
    $nacimiento = busca_edad($pacient->day_of_birth);
    $fecha_actual = date('d') . " de " . $meses[date('n') - 1] . " del " . date('Y');
    $html_personal = '<section class="sheet padding-20mm"><article><table cellspacing="0" cellpadding="0" style="width:100%" >
<tr>
            <td>&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td><B>No</B></td>
            <td><b>' . $user->id . '</b></td>
 </tr>
 <tr>
            <td>&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
 </tr>
 <tr >
            <td>Nombre</td>
            <td><b>' . $pacient->name . " " . $pacient->name . '</b></font> </td>
            <td>Edad</td>
            <td><b>' . $nacimiento . '</b></td>
            <td>Fecha</td>
            <td><b>' . $fecha_actual . '</b></td>
 </tr>
 <tr >
            <td>Estado Civil</td>
            <td><b>' . $pacient->sick . '</b></td>
            <td>Ocupacion</td>
            <td><b>' . $pacient->medicaments . '</b></td>
            <td></td>
            <td></td>
 </tr>
  <tr >
            <td>&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
 </tr>
</table>';

    //signos vitales

    $signal = VitalSignsData::getLastMaxDate($_GET["pacient_id"], $_GET["medic_id"], $user->created_at);
    $html_signal = '<table cellspacing="0" cellpadding="0" style="width:100%" >
<tr>
          
            <td><b>PA</b></td>
            <td>' . $signal->bloodpressureright . '</td>
            <td><b>FC</b></td>
            <td>' . $signal->heartrate . '</td>
            <td><b>FR</b></td>
            <td>' . $signal->breathingfrequency . '</td>
            <td><b>To</td>
            <td>' . $signal->temperature . '</td>
            <td><b>Peso</b></td>
            <td>' . $signal->weight . '</td>
            <td><b>Talla</b></td>
            <td>' . $signal->size . '</td>
             <td><b>Saturacion</b></td>
            <td>' . $signal->saturation . '</td>
 </tr>
 <tr>
          
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
 </tr>
 </table>';

    $html_historial = '<table cellspacing="0" cellpadding="0" style="width:100%" ><tr><td>PACIENTE: </td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>' . $pacient->name . " " . $pacient->name . '</td></tr><tr><td>MEDICO: </td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>' . $medic->name . " " . $medic->lastname . '</td></tr>';
    $html_historial .= '<tr><td>S</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>' . trim($user->s) . '</td></tr>';
    $html_historial .= '<tr><td>O</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>' . trim($user->o) . '</td></tr>';
    $html_historial .= '<tr><td>I</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>' . trim($user->i) . '</td></tr>';
    $html_historial .= '<tr><td>P</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>' . trim($user->p) . '</td></tr>';
    $html_historial .= '<tr><td colspan="4">&nbsp;</td></tr>';
    $html_historial .= '</table>';

    $html_historial = '<table cellspacing="0" cellpadding="0" style="width:100%" >';
    $html_historial .= '<tr><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td><h1>S</h1></td><td>' . $user->s . '</td><td>';
    $html_historial .= '<tr><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td><h1>O</h1></td><td>' . $user->o . '</td><td>';
    $html_historial .= '<tr><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td><h1>I</h1></td><td>' . $user->i . '</td><td>';
    $html_historial .= '<tr><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td><h1>P</h1></td><td>' . $user->p . '</td><td>';
    $html_historial .= '<tr><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>';

    $html_historial .= '</table>';


    $html_historial .= '</article>';

    $html_cuerpo .= $html_personal . $html_signal . $html_historial . '</section>';

}
$html_footer = '';
$html .= $html_header . $html_cuerpo . $html_footer;
$html .= '</body></html>';
$html = preg_replace('/>\s+</', "><", $html);
Dompdf\Autoloader::register();

// reference the Dompdf namespace
use Dompdf\Dompdf;

// instantiate and use the dompdf class
$dompdf = new Dompdf();



$dompdf->loadHtml(utf8_decode($html));

// (Optional) Setup the paper size and orientation
//$dompdf->setPaper('A4', 'landscape');
//$customPaper = array(0,0,606.614,793.700);
$dompdf->setPaper('legal', 'portrait');
// Render the HTML as PDF
$dompdf->render();

$font = $dompdf->getFontMetrics()->get_font("helvetica", "bold");
$dompdf->getCanvas()->page_text(0, 0, "Pagina {PAGE_NUM} de {PAGE_COUNT}", $font, 8, array(0, 0, 0));
//$dompdf->stream("dompdf_out.pdf", array("Attachment" => true));


// Output the generated PDF to Browser
$dompdf->stream();

//echo $html;
?>