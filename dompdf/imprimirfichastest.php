<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 11/28/2018
 * Time: 5:31 PM
 */
session_start();

require_once 'lib/html5lib/Parser.php';
require_once 'lib/php-font-lib/src/FontLib/Autoloader.php';
require_once 'lib/php-svg-lib/src/autoload.php';
require_once 'src/Autoloader.php';


require_once '../core/app/model/HistoryClinicData.php';
require_once '../core/app/model/PacientData.php';
require_once '../core/app/model/MedicData.php';
require_once '../core/app/model/RecipeData.php';
require_once '../core/app/model/VitalSignsData.php';


require_once '../core/controller/Executor.php';
require_once '../core/controller/Database.php';
require_once '../core/controller/Core.php';
require_once '../core/controller/Model.php';


$pacient = PacientData::getById($_GET["pacient_id"]);
$medic = MedicData::getById($_GET["medic_id"]);
$recipe = RecipeData::getLastMax($_GET["pacient_id"], $_GET["medic_id"]);
$signal = VitalSignsData::getLastMax($_GET["pacient_id"], $_GET["medic_id"]);


function dataready($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

$users = array();
if ((isset($_GET["pacient_id"]) && isset($_GET["medic_id"]) && isset($_GET["start_at"]) && isset($_GET["finish_at"])) && ($_GET["pacient_id"] != "" || $_GET["medic_id"] != "" || ($_GET["start_at"] != "" && $_GET["finish_at"] != ""))) {
    $sql = "select * from  historialclinic where ";

    if ($_GET["pacient_id"] != "") {

        $sql .= " pacient_id = " . $_GET["pacient_id"];
    }

    if ($_GET["medic_id"] != "") {
        if ($_GET["pacient_id"] != "") {
            $sql .= " and ";
        }

        $sql .= " medic_id = " . $_GET["medic_id"];
    }


    if ($_GET["start_at"] != "" && $_GET["finish_at"]) {
        if ($_GET["pacient_id"] != "" || $_GET["medic_id"] != "") {
            $sql .= " and ";
        }

        $sql .= " ( DATE(date_at) >= \"" . $_GET["start_at"] . "\" and DATE(date_at) <= \"" . $_GET["finish_at"] . "\" ) ";
    }
    $sql .= " and status_id=1 order by  created_at DESC";
    //    echo $sql;
    $users = HistoryClinicData::getBySQL($sql);

} else {
    $users = HistoryClinicData::getAllPendings();

}


//echo $_GET["id"];
//echo $_GET["pacient_id"];
//echo $_GET["medic_id"];

//echo $pacient->name . " " . $pacient->lastname;

$html_signal = '
<table border=0>
            <tr>
            <th>PA izquierda</th>
            <td>' . $signal->bloodpressureleft . '</td>
            <th>PA derecha</th>
            <td>' . $signal->bloodpressureright . '</td>
            <th>FC</th>
            <td>' . $signal->heartrate . '</td>
            <th>FR</th>
            <td>' . $signal->breathingfrequency . '</td>
            <th>temperatura</th>
            <td>' . $signal->temperature . '</td>
            <th>Peso</th>
            <td>' . $signal->weight . '</td>
            <th>Estatura</th>
            <td>' . $signal->size . '</td>
           </tr>
</table>
';

$html = '<html lang="en">
<head>
  <meta charset="utf-8">
  <title>A4</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">
  <!-- Load paper.css for happy printing -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">
  <!-- Set page size here: A5, A4 or A3 -->
  <!-- Set also "landscape" if you need -->
  <style>@page { size: legal }</style>
  <!-- <style>
    body   { font-family: serif }
    h1     { font-family: \'Tangerine\', cursive; font-size: 40pt; line-height: 18mm}
    h2, h3 { font-family: \'Tangerine\', cursive; font-size: 24pt; line-height: 7mm }
    h4     { font-size: 32pt; line-height: 14mm }
    h2 + p { font-size: 18pt; line-height: 7mm }
    h3 + p { font-size: 14pt; line-height: 7mm }
    li     { font-size: 11pt; line-height: 5mm }
    h1      { margin: 0 }
    h1 + ul { margin: 2mm 0 5mm }
    h2, h3  { margin: 0 3mm 3mm 0; float: left }
    h2 + p,
    h3 + p  { margin: 0 0 3mm 50mm }
    h4      { margin: 2mm 0 0 50mm; border-bottom: 2px solid black }
    h4 + ul { margin: 5mm 0 0 50mm }
    article { border: 4px double black; padding: 5mm 10mm; border-radius: 3mm }
  </style>-->
</head><body class="legal">';
foreach ($users as $user) {
    $pacient = $user->getPacient();
    $medic = $user->getMedic();
    $html .= '<section class="sheet padding-20mm"><article><table cellspacing="0" cellpadding="0"><tr><td>PACIENTE: </td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>' . $pacient->name . " " . $pacient->name . '</td></tr><tr><td>MEDICO: </td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>' . $medic->name . " " . $medic->lastname . '</td></tr>';
    $html .= '<tr><td>S</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>' . trim($user->s) . '</td></tr>';
    $html .= '<tr><td>O</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>' . trim($user->o) . '</td></tr>';
    $html .= '<tr><td>I</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>' . trim($user->i) . '</td></tr>';
    $html .= '<tr><td>P</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>' . trim($user->p) . '</td></tr>';
    $html .= '<tr><td colspan="4">&nbsp;</td></tr>';
    $html .= '</table>';
    $html .= '</article></section>';
}
$html .= '</body></html>';
//$html = $html_signal . $html;


Dompdf\Autoloader::register();

// reference the Dompdf namespace
use Dompdf\Dompdf;

// instantiate and use the dompdf class
$dompdf = new Dompdf();


$dompdf->loadHtml(utf8_decode($html));

// (Optional) Setup the paper size and orientation

$dompdf->setPaper('legal', 'landscape');

// Render the HTML as PDF
$dompdf->render();


// Output the generated PDF to Browser
//$dompdf->stream();
$pdf = $dompdf->stream("my_pdf.pdf", array("Attachment" => 0));

//echo $html;
?>