<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 11/28/2018
 * Time: 5:31 PM
 */
session_start();

require_once 'lib/html5lib/Parser.php';
require_once 'lib/php-font-lib/src/FontLib/Autoloader.php';
require_once 'lib/php-svg-lib/src/autoload.php';
require_once 'src/Autoloader.php';


require_once '../core/app/model/HistoryClinicData.php';
require_once '../core/app/model/PacientData.php';
require_once '../core/app/model/MedicData.php';
require_once '../core/app/model/RecipeData.php';
require_once '../core/app/model/VitalSignsData.php';


require_once '../core/controller/Executor.php';
require_once '../core/controller/Database.php';
require_once '../core/controller/Core.php';
require_once '../core/controller/Model.php';


$pacient = PacientData::getById($_GET["pacient_id"]);
$medic = MedicData::getById($_GET["medic_id"]);
$recibo = HistoryClinicData::getById($_GET["id"]);
$recipe = RecipeData::getLastMax($_GET["pacient_id"], $_GET["medic_id"]);
$signal = VitalSignsData::getLastMax($_GET["pacient_id"], $_GET["medic_id"]);

//echo $_GET["id"];
//echo $_GET["pacient_id"];
//echo $_GET["medic_id"];

//echo $pacient->name . " " . $pacient->lastname;
$html_header = '<html lang="en">
<head>
  <meta charset="utf-8">
  <title>A4</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">
  <!-- Load paper.css for happy printing -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">
  <!-- Set page size here: A5, A4 or A3 -->
  <!-- Set also "landscape" if you need -->
  <style>@page { size: legal } </style>
  <style>
 .rTable {
  	display: table;
  	width: 100%;
}
.rTableRow {
  	display: table-row;
}
.rTableHeading {
  	display: table-header-group;
  	background-color: #ddd;
}
.rTableCell, .rTableHead {
  	display: table-cell;
  	padding: 3px 10px;
  	border: 1px solid #999999;
}
.rTableHeading {
  	display: table-header-group;
  	background-color: #ddd;
  	font-weight: bold;
}
.rTableFoot {
  	display: table-footer-group;
  	font-weight: bold;
  	background-color: #ddd;
}
.rTableBody {
  	display: table-row-group;
}

  </style>
  
</head><body class="legal" ><section class="sheet padding-20mm"><article>';

$html_signal = '<div class="rTable">
<div class="rTableRow">
            <div class="rTableCell"><B>PACIENTE</B></div>
            <div class="rTableCell"><b>' . $pacient->name . " " . $pacient->name . '</b></font> </div>
 </div>
<div class="rTableRow">
            <div class="rTableCell">PA izquierda</div>
            <div class="rTableCell">' . $signal->bloodpressureleft . '</div>
            <div class="rTableCell">PA derecha</div>
            <div class="rTableCell">' . $signal->bloodpressureright . '</div>
            <div class="rTableCell">FC</div>
            <div class="rTableCell">' . $signal->heartrate . '</div>
            <div class="rTableCell">FR</div>
            <div class="rTableCell">' . $signal->breathingfrequency . '</div>
            <div class="rTableCell">temperatura</div>
            <div class="rTableCell">' . $signal->temperature . '</div>
            <div class="rTableCell">Peso</div>
            <div class="rTableCell">' . $signal->weight . '</div>
            <div class="rTableCell">Estatura</div>
            <div class="rTableCell">' . $signal->size . '</div>
 </div></div>';

$html_ficha = '<table cellspacing="0" cellpadding="0">';
$html_ficha .= '<tr><td>S</td><td>' . $recibo->s . '</td><td>';
$html_ficha .= '<tr><td>O</td><td>' . $recibo->o . '</td><td>';
$html_ficha .= '<tr><td>I</td><td>' . $recibo->i . '</td><td>';
$html_ficha .= '<tr><td>P</td><td>' . $recibo->p . '</td><td>';
$html_ficha .= '</table><div style="clear: both;">';

$html_footer = '</article></section></body></html>';

$html = $html_header . $html_signal . $html_ficha . $html_footer;


Dompdf\Autoloader::register();

// reference the Dompdf namespace
use Dompdf\Dompdf;

// instantiate and use the dompdf class
$dompdf = new Dompdf();


$dompdf->loadHtml(utf8_decode($html));

// (Optional) Setup the paper size and orientation
//$dompdf->setPaper('A4', 'landscape');
//$customPaper = array(0,0,606.614,793.700);
$dompdf->setPaper('legal', 'portrait');
// Render the HTML as PDF
$dompdf->render();


// Output the generated PDF to Browser
$dompdf->stream();

//echo $html;
?>