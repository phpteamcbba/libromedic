<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 11/28/2018
 * Time: 5:31 PM
 */
session_start();

require_once 'lib/html5lib/Parser.php';
require_once 'lib/php-font-lib/src/FontLib/Autoloader.php';
require_once 'lib/php-svg-lib/src/autoload.php';
require_once 'src/Autoloader.php';


require_once '../core/app/model/HistoryClinicData.php';
require_once '../core/app/model/PacientData.php';
require_once '../core/app/model/MedicData.php';
require_once '../core/app/model/RecipeData.php';
require_once '../core/app/model/VitalSignsData.php';


require_once '../core/controller/Executor.php';
require_once '../core/controller/Database.php';
require_once '../core/controller/Core.php';
require_once '../core/controller/Model.php';


$pacient = PacientData::getById($_GET["pacient_id"]);
$medic = MedicData::getById($_GET["medic_id"]);
$recibo = HistoryClinicData::getById($_GET["id"]);
$recipe = RecipeData::getById($_GET["id"]);

//echo $_GET["id"];
//echo $_GET["pacient_id"];
//echo $_GET["medic_id"];

//echo $pacient->name . " " . $pacient->lastname;


$html_recipe = '<html lang="en">
<head>
  <meta charset="utf-8">
  <style>
  @page {
    margin: 0;
  }
  *{
    color: #111;
  }
  body {
    font-size: 11pt;
    padding: 0;
    margin: 0;
  }
  .contenedor{
    background-repeat: no-repeat;
    background-position: 0 0;
  }
  .background{
    position: fixed;
    top: 0;
    left: 0;
    width: 813.229pt;
    height: 507.401pt;
    z-index: 1;
  }
  .background img{
    width: 100%;
    height: auto;
  }
  .cuerpo{
    page-break-after: auto;
    max-width: 396.850pt;
    max-height: 606.614pt;
    z-index: 10;
    margin: 113.385pt 28.346pt 28.346pt 56.692pt;
  }

  tr  { page-break-before: auto; max-height: 50pt; }
  td{
    border-bottom: 1px dotted rgba(0,0,0,0.04);
  }
  #duracion, #completado{ display: none; }
  </style>
</head><body> 
 <div class=contenedor>
      <div class=cuerpo>
<table class=table style=border: 0;>
  <tr>
  <td align=right>&nbsp;</td>
  <td><b>' . $pacient->name . " " . $pacient->name . '</b></td>
  </tr>
  <tr>
  <td>&nbsp;</td><td nowrap>' . $recipe->recipe_pdf . '</td>
  </tr>
</table>
</div></div>
';

$html = $html_recipe;


// reference the Dompdf namespace
Dompdf\Autoloader::register();

// reference the Dompdf namespace
use Dompdf\Dompdf;

// instantiate and use the dompdf class
$dompdf = new Dompdf();
$dompdf->loadHtml(utf8_decode($html));

// (Optional) Setup the paper size and orientation
$customPaper = array(0, 0, 606.614, 793.700);
$dompdf->setPaper($customPaper, 'portrait');

// Render the HTML as PDF
$dompdf->render();


// Output the generated PDF to Browser
//$dompdf->stream();


$font = $dompdf->getFontMetrics()->get_font("helvetica", "bold");
$dias = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

$dompdf->getCanvas()->page_text(150, 560, "        " . date('d') . "    " . $meses[date('n') - 1] . "     " . date('Y'), $font, 10, array(0, 0, 0));
//$dompdf->stream("dompdf_out.pdf", array("Attachment" => false));
$dompdf->stream();
//echo $html;
?>