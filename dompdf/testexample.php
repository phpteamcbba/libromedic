<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 11/28/2018
 * Time: 5:31 PM
 */
session_start();

require_once 'lib/html5lib/Parser.php';
require_once 'src/Autoloader.php';


$html = $_SESSION["report_recibo"];


Dompdf\Autoloader::register();

// reference the Dompdf namespace
use Dompdf\Dompdf;

// instantiate and use the dompdf class
$dompdf = new Dompdf();
$dompdf->loadHtml(utf8_decode($html));

// (Optional) Setup the paper size and orientation
$customPaper = array(0, 0, 396, 606);
$dompdf->setPaper($customPaper, 'portrait');

// Render the HTML as PDF
$dompdf->render();


// Output the generated PDF to Browser
//$dompdf->stream();


$font = $dompdf->getFontMetrics()->get_font("helvetica", "bold");
$dias = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

$dompdf->getCanvas()->page_text(150, 560, "        " . date('d') . "    " . $meses[date('n') - 1] . "     " . date('Y'), $font, 10, array(0, 0, 0));
//$dompdf->stream("dompdf_out.pdf", array("Attachment" => false));
$dompdf->stream();

//echo $html;
?>