﻿<?php


//require('fpdf.php');
require('html2pdf.php');
include "../core/autoload.php";
include "../core/app/model/PacientData.php";
include "../core/app/model/MedicData.php";
include "../core/app/model/RecipeData.php";

session_start();


class PDF extends FPDF
{

// Cargar los datos
    function LoadData()
    {
        $alumns = RecipeData::getById($_GET["id"]);
        $pacients = PacientData::getById($_GET["p"]);
        $medics = MedicData::getById($_GET["m"]);
        $data = array();
        $data[] = $pacients->name . " " . $pacients->lastname;;
        $data[] = $medics->name . " " . $medics->lastname;;
        $data[] = $alumns->recipe;
        $data[] = $alumns->created_at;
        $data[] = $alumns->date_at;
        $data[] = $alumns->time_at;

        //var_dump($data);
        return $data;
    }

    function Header()
    {
        $this->SetFont('Arial', 'B', 15);
        $this->setX(20);

// $this->Line(20, 6, 195, 6); // 20mm from each edge

//$this->Line(20, 260.5, 200, 260.5); // 20mm from each edge
//$this->Line(20, 261.5, 200, 261.5); // 20mm from each edge
// $this->Line(20, 262.5, 200, 262.5); // 20mm from each edge
        $this->SetFont('Arial', 'B', 30);
        $this->Cell(200, 10, "AutoMotriz");
        $this->Ln();
        $this->SetX(75);
        $this->Cell(200, 10, "Integral");
        $this->setY(7);
        $this->SetFont('Arial', 'B', 10);
        $this->setX(165 - 5);
        $this->Cell(39, 6, "ORDEN DE SERVICIO", 1);
        $this->setY(13);
        $this->setX(165 - 5);
        $this->Cell(39, 6, "", 1);
        $this->setY(19);
        $this->setX(160);
        $this->Cell(13, 6, "DIA", 1);
        $this->setX(160 + 13);
        $this->Cell(13, 6, "MES", 1);
        $this->setX(160 + 26);
        $this->Cell(13, 6, "A" . utf8_decode("Ñ") . "O", 1);
        $this->setY(25);
        $this->setX(160);
        $this->Cell(13, 6, "", 1);
        $this->setX(160 + 13);
        $this->Cell(13, 6, "", 1);
        $this->setX(160 + 26);
        $this->Cell(13, 6, "", 1);

    }

// Tabla simple
    function ImprovedTable($data)
    {
        $this->setY(31);
        $this->setX(20);
        $this->SetFont('Arial', 'B', 8);
        $this->setY(33);
        $this->setX(20);
        $this->Cell(0, 10, "AV. CESAR SANDINO NO. 313 (FRENTE AL SEGURO SOCIAL) COL. 1RO. DE MAYO VILLAHERMOSA, TABASCO. TEL: 352 29 10");
        $this->setY(40);
        $this->setX(20);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(0, 35, "", 1);
        $this->setY(38);
        $this->setX(20);
        $this->Cell(0, 10, " PACIENTE ________________________________________________________________________________________________________");
        $this->setY(43);
        $this->setX(20);
        $this->Cell(0, 10, " MEDICO ______________________________________________________________________________________________________");
        $this->setY(48);
        $this->setX(20);

        $this->Cell(0, 10, " RECETA ________________________________" . $data[2]);
        $this->setY(53);
        $this->setX(20);

    }

// Tabla coloreada
}


$pdf = new PDF();
$pdf->AddPage();
$datas = array();
$datas = $pdf->LoadData();
$pdf->ImprovedTable($datas);
$pdf->Output();
//print "<script>window.location=\"".$name."\";</script>";


?>