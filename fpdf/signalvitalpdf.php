﻿<?php

require('fpdf.php');
include "../core/autoload.php";
include "../core/app/model/PacientData.php";
include "../core/app/model/MedicData.php";
include "../core/app/model/VitalSignsData.php";
session_start();


class PDF extends FPDF
{

// Load data
    function LoadData()
    {
        $alumns = $_SESSION["report_data_signal"];
        $data = array();
        $x = 1;
        foreach ($alumns as $al) {

            $data1 = array();
            $medic = $al->getMedic();
            $pacient = $al->getPacient();
            $data1[] = $pacient->name . " " . $pacient->lastname;;
            $data1[] = $medic->name . " " . $medic->lastname;;
            $data1[] = $al->bloodpressureleft;
            $data1[] = $al->bloodpressureright;
            $data1[] = $al->heartrate;
            $data1[] = $al->temperature;
            $data1[] = $al->weight;
            $data1[] = $al->size;
            $data1[] = $al->IMC;
            $data1[] = $al->saturation;

            $data[] = $data1;


        }
        return $data;
    }

// Simple table
    function BasicTable($header, $data)
    {
        // Header
        foreach ($header as $col)
            $this->Cell(40, 7, $col, 1);
        $this->Ln();
        // Data
        foreach ($data as $row) {
            foreach ($row as $col)
                $this->Cell(40, 6, $col, 1);
            $this->Ln();
        }
    }

// Better table
    function ImprovedTable($header, $data)
    {
        // Column widths
        $w = array(10, 15, 10, 15, 10, 15, 10, 15, 10, 35);
        // Header
        for ($i = 0; $i < count($header); $i++)
            $this->Cell($w[$i], 7, $header[$i], 1, 0, 'C');
        $this->Ln();
        // Data
        foreach ($data as $row) {
            $this->Cell($w[0], 6, $row[0], 'LR');
            $this->Cell($w[1], 6, $row[1], 'LR');
            $this->Cell($w[2], 6, $row[2], 'R');
            $this->Cell($w[3], 6, $row[3], 'LR', 0, 'R');

            $this->Cell($w[4], 6, $row[4], 'LR', 0, 'R');
            $this->Cell($w[5], 6, $row[5], 'LR', 0, 'R');
            $this->Cell($w[6], 6, $row[6], 'LR', 0, 'R');
            $this->Cell($w[7], 6, $row[7], 'LR', 0, 'R');

            $this->Cell($w[8], 6, $row[8], 'LR', 0, 'R');
            $this->Cell($w[9], 6, $row[9], 'LR');


            $this->Ln();
        }
        // Closing line
        $this->Cell(array_sum($w), 0, '', 'T');
    }

// Colored table
    function FancyTable($header, $data)
    {
        // Colors, line width and bold font
        $this->SetFillColor(255, 0, 0);
        $this->SetTextColor(255);
        $this->SetDrawColor(128, 0, 0);
        $this->SetLineWidth(.1);
        $this->SetFont('', 'B');
        // Header
        $w = array(10, 15, 10, 15, 10, 15, 10, 15, 10, 35);
        for ($i = 0; $i < count($header); $i++)
            $this->Cell($w[$i], 7, $header[$i], 1, 0, 'C', true);
        $this->Ln();
        // Color and font restoration
        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->SetFont('');
        // Data
        $fill = false;
        foreach ($data as $row) {
            $this->Cell($w[0], 6, $row[0], 'LR', 0, 'L', $fill);
            $this->Cell($w[1], 6, $row[1], 'LR', 0, 'L', $fill);
            $this->Cell($w[2], 6, $row[2], 'LR', 0, 'R', $fill);
            $this->Cell($w[3], 6, $row[3], 'LR', 0, 'R', $fill);

            $this->Cell($w[4], 6, $row[4], 'LR', 0, 'R', $fill);
            $this->Cell($w[5], 6, $row[5], 'LR', 0, 'R', $fill);
            $this->Cell($w[6], 6, $row[6], 'LR', 0, 'R', $fill);
            $this->Cell($w[7], 6, $row[7], 'LR', 0, 'R', $fill);

            $this->Cell($w[8], 6, $row[8], 'LR', 0, 'R', $fill);
            $this->Cell($w[9], 6, $row[9], 'LR', 0, 'R', $fill);


            $this->Ln();
            $fill = !$fill;
        }
        // Closing line
        $this->Cell(array_sum($w), 0, '', 'T');
    }
}

ob_start();

$pdf = new PDF('L', 'mm', array(300, 350));
// Column headings
$header = array('Paciente', 'Medico', 'bloodpressureleft', 'bloodpressureright', 'heartrate', 'temperature', 'weight', 'size', 'IMC', 'Saturacion');
// Data loading
$data = $pdf->LoadData();
$pdf->SetFont('Arial', '', 10);
$pdf->AddPage();
$pdf->BasicTable($header, $data);
$pdf->Output();
ob_end_flush();
?>