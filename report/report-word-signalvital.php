<?php
session_start();
include "../core/autoload.php";
include "../core/app/model/PacientData.php";
include "../core/app/model/MedicData.php";
include "../core/app/model/StatusData.php";
include "../core/app/model/VitalSignsData.php";

require_once '../PhpWord/Autoloader.php';

use PhpOffice\PhpWord\Autoloader;

Autoloader::register();

$word = new  PhpOffice\PhpWord\PhpWord();

$alumns = $_SESSION["report_data_signal"];

$section1 = $word->AddSection();
$section1->addText("REPORTE DE SIGNOS VITALES", array("size" => 22, "bold" => true, "align" => "right"));


$styleTable = array('borderSize' => 6, 'borderColor' => '888888', 'cellMargin' => 40);
$styleFirstRow = array('borderBottomColor' => '0000FF', 'bgColor' => 'AAAAAA');

$table1 = $section1->addTable("table1");
$table1->addRow();
$table1->addCell()->addText("Paciente");
$table1->addCell()->addText("Medico");
$table1->addCell()->addText("bloodpressureleft");
$table1->addCell()->addText("bloodpressureright");
$table1->addCell()->addText("heartrate");
$table1->addCell()->addText("temperature");
$table1->addCell()->addText("weight");
$table1->addCell()->addText("size");
$table1->addCell()->addText("IMC");
$table1->addCell()->addText("created_at");
$table1->addCell()->addText("date_at");
$table1->addCell()->addText("time_at");


$total = 0;
foreach ($alumns as $al) {
    $medic = $al->getMedic();
    $pacient = $al->getPacient();

    $table1->addRow();

    $table1->addCell(3000)->addText($pacient->name . " " . $pacient->lastname);
    $table1->addCell(3000)->addText($medic->name . " " . $medic->lastname);
    $table1->addCell(3000)->addText($al->bloodpressureleft);
    $table1->addCell(3000)->addText($al->bloodpressureright);
    $table1->addCell(3000)->addText($al->heartrate);
    $table1->addCell(3000)->addText($al->temperature);
    $table1->addCell(3000)->addText($al->weight);
    $table1->addCell(3000)->addText($al->size);
    $table1->addCell(3000)->addText($al->IMC);
    $table1->addCell(3000)->addText($al->created_at);
    $table1->addCell(3000)->addText($al->date_at);
    $table1->addCell(3000)->addText($al->time_at);

}

//$section1->addText("TOTAL: $ ".number_format($total,2,".",","),array("size"=>18));


$word->addTableStyle('table1', $styleTable, $styleFirstRow);
/// datos bancarios
$section1->addText("");
$section1->addText("");
$section1->addText("");
$section1->addText("Generado por BookMedik v2.0");
$filename = "report-" . time() . ".docx";
#$word->setReadDataOnly(true);
$word->save($filename, "Word2007");
//chmod($filename,0444);
header("Content-Disposition: attachment; filename='$filename'");
readfile($filename); // or echo file_get_contents($filename);
unlink($filename);  // remove temp file


?>